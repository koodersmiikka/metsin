var express     = require('express')
var bodyParser  = require('body-parser')
var morgan      = require('morgan')
var serveStatic = require('serve-static')
var fs          = require('fs')
var xml2json = require('xml2json');
var xpath   = require('xpath');
var dom     = require('xmldom').DOMParser;

var port = 8000;
var xmlSource = 'testdata/MvKuviot_estate_lite.xml'
var jsonPath = 'testdata/MvKuviot-utf8.json'

var app = express()
app.use(morgan('tiny'))
app.use(serveStatic('front', {'index': ['index.html']}))

// when client POST data with Content-Type: application/json, make content available at req.body as JSON.parse() would
app.use(bodyParser.json())

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next();
});

app.post('/', function (req, res) {
    console.log('body:', req.body)
    var search = req.body.search || "0"
    var out = getEstatesAndStands(search)
    console.log(out, search)
    res.send(out)
    res.end()
});

var json_data

if (fs.existsSync(jsonPath)) {
    console.log("Converted data found, loading...")
    data = fs.readFileSync(jsonPath, 'utf8')
} else {
    console.log("Reading huge XML to memory. Please suffer.")
    var xml_data = fs.readFileSync(xmlSource, 'utf8')
    console.log("Converting...")
    var data = xml2json.toJson(xml_data)
    fs.writeFileSync(jsonPath, data, 'utf8')
}

data = JSON.parse(data)

console.log("Doned.")

app.listen(port);

const getRealEstates = id => data['ForestPropertyData']['re:RealEstates']['re:RealEstate']
  .filter(e => e['re:RegisterUnitId'] === id)
const getStands = id => data['ForestPropertyData']['st:Stands']['st:Stand']
  .filter(e => e['realEstateId'] === id)

function getEstatesAndStands(id) {
  const estates = getRealEstates(id)
  const stands = estates
    .map(e => e.id)
    .map(id => getStands(id))
  return {
    estates: estates,
    stands: stands,
    success: Object.keys(estates).length > 0
  }
}

/*
var xml_data = fs.readFileSync('testdata/MvKuviot-utf8.xml', 'utf8');
const createRegExp1 = id => new RegExp(`<re:RealEstate[^>]+?id="(\\d+)"[^>]*?>[\\s\\S]+?:RegisterUnitId>${id}<[\\s\\S]+?<\/re:RealEstate>`, 'g')
const createRegExp2 = id => new RegExp(`<st:Stand[^>]+?realEstateId="${id}"[\\s\\S]+?<\/st:Stand>`, 'g')
function getStand(num){
    var re = createRegExp1(num)
    var realEstate, stand
    while (match = re.exec(xml_data)) {
        realEstate = JSON.parse(xml2json.toJson(match[0]))['re:RealEstate']
    }
    if (realEstate) {
        re = createRegExp2(realEstate['id'])
        while (match = re.exec(xml_data)) {
            stand = JSON.parse(xml2json.toJson(match[0]))
        }
        return {realEstate:realEstate, stand:stand, success:true}
    }
    return {success:false}
}
*/
