# metsin 0.1h
Forestry data browser/viewer

# Installation
```
yaourt -S --noconfirm nodejs
git clone https://gitlab.com/koodersmiikka/metsin.git
npm install
npm install nodemon //this is not mandatory, but helps when developing
```
# Setting up data and running the server
Put the xml formatted data into a folder called testdata.
### Run the server:
With nodemon
```
./node_modules/.bin/nodemon app.js
```
Without
```
node app.js
```
First time conversion of the data takes a while, so be patient.
After the conversion the server should be running at the port 8000

# Information
## Sources and used libraries
https://threejs.org/docs/
https://github.com/ubilabs/google-maps-api-threejs-layer
https://www.npmjs.com/package/xml2json
https://github.com/proj4js/proj4js

## Dem obscure maprefs
http://latuviitta.org/documents/YKJ-TM35FIN_muunnos_ogr2ogr_cs2cs.txt
http://www.georeference.org/forum/e31332F3132333431312F746F77677338342E646F6378/towgs84.docx

## About
Authored by Miikka Kostian & Harri Karhu. 
Made in approximately 25 hours for forestry hackaton on 12.05.2017. 
Sorry for thr messy/incomplete code and the lack of documentation. 
Licenced under Apache 2.0
