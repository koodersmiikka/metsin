$('#searchForm').submit(function(e){
    e.preventDefault();

    const haku = $('#hakualue').val();

    $.ajax({
        type: "POST",
        url: '/',
        contentType: 'application/json',
        data: '{ "search" : "'+haku+'" }',
        success: function(serverResponse){
            if(serverResponse['success']){
                $('.actuallyhasdata').removeClass('hide');
                console.log("Onnistui!");
                populateAll(serverResponse);
            } else {
                $('#otsikko').html("Ei löytynyt mehtää kiinteistötunnuksella "+haku);
                console.log("Failasin uguuu");
                $('.actuallyhasdata').addClass('hide');
            }
        },
        error:function(exception){ console.log(exception); }
    });
});

var mappirajat = [];
var highlight = null;

function populateAll(data){

    mappirajat = [];

    $('#infotabs').html('');
    $('#infocontent').html('');
    $('.tab-pane').html('');

    for(var i = 0; i < data['estates'].length;i++){
        $('#otsikko').html(data['estates'][i]['re:RealEstateName']+' - '+kuntakoodi[data['estates'][i]['re:LocationMunicipalityNumber']]+' ('+data['estates'][i]['re:RegisterUnitId']+')<br>');
    }

    for(var i = 0; i < data['stands'].length; i++){
        for(var x = 0; x < data['stands'][i].length; x++){
                var standi = data['stands'][i][x];
                console.log(standi);
                // Laatikot
                $('#infotabs').append('<li'+(x == 0 ? ' class="active"':'') + '><a onclick="highlightMap('+ x + ')" data-toggle="tab" href="#'+standi['id']+'">Kuvio '+(x+1)+'</a></li>');
                $('#infocontent').append('<div id="'+standi['id']+'" class="tab-pane fade in '+(x == 0 ? 'active' : '')+'"><table class="table" id="'+standi['id']+'table"></table></div>');

                doTabsContent(standi,x);
                mappirajat.push(standi['st:StandBasicData']['gdt:PolygonGeometry']);
        }
    }
    doMap(mappirajat);


    $('[href="#'+data['stands'][0][0]['id']+"\"]").click();

    // console.log(data);
    //console.log(data['re:RealEstate']['re:RealEstateName']);
}

function convertDate(dateString) {
    var date = new Date(dateString);
    return (date.getDate() < 10 ? '0':'')+date.getDate()+"."+(date.getMonth() < 10 ? '0':'')+date.getMonth()+"."+date.getFullYear();
}

function doTabsContent(standi,alue){
    // const info = standi['st:StandBasicData'];
    // Laatikoiden sisältö

    if(standi['st:StandBasicData']['co:DataSource']){
        $('#'+standi['id']+'table').append('<tr><td>Tietolähde</td><td>'+DataSource[standi['st:StandBasicData']['co:DataSource']]+'</td></tr>');
    }
    if(standi['st:StandBasicData']['st:StandBasicDataDate']){
        $('#'+standi['id']+'table').append('<tr><td>Mitattu</td><td>'+convertDate(standi['st:StandBasicData']['st:StandBasicDataDate'])+'</td></tr>');
    }
    if(standi['st:StandBasicData']['st:Area']){
        $('#'+standi['id']+'table').append('<tr><td>Pinta-ala</td><td>'+standi['st:StandBasicData']['st:Area']+' ha</td></tr>');
    }
    if(standi['st:StandBasicData']['st:Accessibility']){
        $('#'+standi['id']+'table').append('<tr><td>Saavutettavuus</td><td>'+Accessibility[standi['st:StandBasicData']['st:Accessibility']]+'</td></tr>');
    }
    if(standi['st:StandBasicData']['st:MainTreeSpecies']){
        $('#'+standi['id']+'table').append('<tr><td>Pääpuulaji</td><td>'+MainTreeSpecies[standi['st:StandBasicData']['st:MainTreeSpecies']]+'</td></tr>');
    }
    if(standi['st:StandBasicData']['st:SoilType']){
        $('#'+standi['id']+'table').append('<tr><td>Maalaji</td><td>'+SoilType[standi['st:StandBasicData']['st:SoilType']]+'</td></tr>');
    }
    /*for(var key in info) {
        $('#'+standi['id']+'table').append('<tr><td>'+getTranslation[1]+'</td><td>'+getTranslation[1]info[key]+'</td></tr>');
    }*/

    //console.log(standi['st:StandBasicData']);

    // Operaatio taulu
    if(standi['op:Operations']){
        for(var i=0;i<standi['op:Operations']['op:Operation'].length;i++){
            const type = standi['op:Operations']['op:Operation'][i]['mainType']
            const subtype = standi['op:Operations']['op:Operation'][i]['op:OperationType']
            const proptype = standi['op:Operations']['op:Operation'][i]['op:ProposalData']['op:ProposalType']
            const propyear = standi['op:Operations']['op:Operation'][i]['op:ProposalData']['op:ProposalYear']

            var type_desc
            var subtype_desc
            var subsubtype_desc = ""
            var date
            var proptype_desc = ehdotustyyppi[proptype]

            console.log(proptype)

            if(type == "1"){
                type_desc = "Hakkuu"
                subtype_desc = HackType[subtype]
            } else if (type == "2"){
                type_desc = "Metsänhoito"
                subtype_desc = CareType[subtype]
            }

            // if(standi['op:Operations']['op:Operation'][i]['mainType']['op:Cutting']){
            //     const leikataan = standi['op:Operations']['op:Operation'][i]['mainType']['op:Cutting']['op:CuttingVolume'];
            // }

            $('#mitateet').append('<tr class="metsanro'+alue+'"><td>'+(alue+1)+'</td><td>'+proptype_desc+' - '+propyear+'</td><td>'+type_desc+': '+subtype_desc+'</td><td>'+subsubtype_desc+'</td></tr>');
        }
    }
}

//ETRS-TM35FIN
proj4.defs('EPSG:3067', '+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs')
const source = new proj4.Proj('EPSG:3067')
const dest = new proj4.Proj('WGS84')

const map = new google.maps.Map(document.getElementById('kartta'), {
        zoom: 15,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        streetViewControl: false,
        styles: map_styles
    })

var layer = new ThreejsLayer({ map: map }, function(l){layer = l})

// Piirtää Initial kartan ja polygonGeometry muuttujassa kaikki polygonigeometria
function doMap(polygonGeometry){
    var color = new THREE.Color("hsl(0, 50%, 50%)")
    var avgcentroid = null
    for(var i=0; i < polygonGeometry.length; i++){
        var centroidProj = polygonGeometry[i]['gml:pointProperty']['gml:Point']['srsName']
        var centroid = polygonGeometry[i]['gml:pointProperty']['gml:Point']['gml:coordinates'].split(',')
        if(avgcentroid == null){
            avgcentroid = [+centroid[0], +centroid[1]];
        } else {
            avgcentroid = [(+centroid[0]+avgcentroid[0])*0.5, (+centroid[1]+avgcentroid[1])*0.5]
        }
        layer.add(makeOutline(polygonGeometry[i], false));
    }
    var c = proj4.transform(source,dest,avgcentroid)
    map.setCenter(new google.maps.LatLng(c['y'],c['x']))
}

function makeOutline(data, fill){
    var polygonProj = data['gml:polygonProperty']['gml:Polygon']['srsName']
    var verts = data['gml:polygonProperty']['gml:Polygon']['gml:exterior']['gml:LinearRing']['gml:coordinates'].split(' ')
    var geo = new THREE.Geometry()
    for(var i=0; i < verts.length; i++){
        var g = proj4.transform(source, dest, verts[i].split(','))
        geo.vertices.push(layer.fromLatLngToVertex(new google.maps.LatLng(g['y'],g['x'])));
    }
    var material = null;
    if(fill){
        material = new THREE.LineBasicMaterial({ color: 0xFF77FF, linewidth: 3 })
    } else {
        material = new THREE.LineBasicMaterial({ color: 0x77FF77, linewidth: 1 })
    }
    return new THREE.Line(geo, material)
}

// Tilan highlightaukseen, mappirajat muuttujassa pelkät mappirajat
function highlightMap(standId){

    console.log(mappirajat)

    $('#mitateet tr').removeClass('success');
    $('.metsanro'+standId).addClass('success');

    var centroidProj = mappirajat[standId]['gml:pointProperty']['gml:Point']['srsName']
    var centroid = mappirajat[standId]['gml:pointProperty']['gml:Point']['gml:coordinates'].split(',')
    var c = proj4.transform(source,dest,centroid)
    map.setCenter(new google.maps.LatLng(c['y'],c['x']))

    if(highlight != null) layer.scene.remove(highlight)
    highlight = makeOutline(mappirajat[standId], true);
    layer.add(highlight)
}
